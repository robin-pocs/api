import { Module } from '@nestjs/common';
import { HeaderResolver, I18nModule } from 'nestjs-i18n';
import path from 'path';

@Module({
    imports: [
        I18nModule.forRoot({
            fallbackLanguage: 'fr',
            loaderOptions: {
              path: path.join(__dirname, '../i18n/'),
              watch: true,
            },
            resolvers: [
                new HeaderResolver(['x-custom-lang'])
            ]
          }),
    ],
    exports: [I18nModule]
})
export class TranslationModule {}
