import bcrypt from 'bcryptjs'
import { Injectable } from '@nestjs/common';
import { error } from 'console';

@Injectable()
export class PasswordService {
    async hash(password: string): Promise<string> {
        const salt = await(bcrypt.genSalt(10));
        return await bcrypt.hash(password, salt);
        
    }
    async verify(password: string, hash: string): Promise<string> {
        return await bcrypt.compare(password, hash)
    }
}
