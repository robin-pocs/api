import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { DocModule } from './doc/doc.module';
import { UsersModule } from './users/users.module';
import { TranslationModule } from './translation/translation.module';
import { PasswordModule } from './password/password.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [DatabaseModule, UsersModule, DocModule, TranslationModule, PasswordModule, AuthModule],
  exports: [DatabaseModule, TranslationModule, PasswordModule]
})
export class AppModule {}
