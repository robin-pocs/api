import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { EmailUnique } from './validation/email-unique';
import { EntityManager, Repository } from 'typeorm';
import { getFromContainer } from 'class-validator';
import { I18nService } from 'nestjs-i18n';
import { PasswordModule } from 'src/password/password.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), PasswordModule],
  controllers: [UsersController],
  providers: [EmailUnique],
  exports: [EmailUnique]
})
export class UsersModule {

  constructor(private readonly i18n: I18nService,private readonly entityManager: EntityManager) {
    const emailUnique = getFromContainer(EmailUnique);
    emailUnique.setRepository(this.entityManager.getRepository(User));
    emailUnique.setI18n(i18n);
  }
}
