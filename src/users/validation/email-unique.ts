import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { EntityManager, Repository, getManager } from 'typeorm';
import { User } from '../entities/user.entity';
import { Inject, Injectable, OnModuleInit, Scope } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { ModuleRef } from '@nestjs/core';
import { I18nService } from 'nestjs-i18n';



@ValidatorConstraint({ name: 'email-unique', async: true })
@Injectable()
export class EmailUnique implements ValidatorConstraintInterface {

  private  userRepository: Repository<User>
  private i18n: I18nService;

    constructor(){}

    public setRepository(userRepository: Repository<User>) {
      this.userRepository = userRepository;
    }

    public setI18n(i18n: I18nService) {
      this.i18n = i18n;
    }

  async validate(email: string, args: ValidationArguments) {
    return !(await this.userRepository.exists({where: {email}}))
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return this.i18n.t('users.validation.email-unique')
    return;
  }
}