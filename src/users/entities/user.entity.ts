import { ApiProperty } from '@nestjs/swagger';
import { OmitType, PartialType } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsNumberString, Validate } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { EmailUnique } from '../validation/email-unique';
import { i18nValidationMessage } from 'nestjs-i18n';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  @IsNumberString()
  id: number;

  @Column()
  @ApiProperty()
  @IsNotEmpty({message: 'validation.NOT_EMPTY'})
  @IsEmail({},{message: 'validation.INVALID_EMAIL'})
  @Validate(EmailUnique)
  email: string;

  @Column()
  @ApiProperty()
  @IsNotEmpty({message: i18nValidationMessage('validation.NOT_EMPTY',{property: 'mot de passe'})})
  password: string;

  @CreateDateColumn()
  @ApiProperty()
  createdAt: Date;

  @Column()
  @ApiProperty()
  lastLogin: Date;

}

export class ReadUser extends OmitType(User, ["password"]){}


export class CreateUser extends OmitType(User,["id", "createdAt", "lastLogin"]){}


export class UpdateUser extends PartialType(CreateUser) {}
