import bcrypt from 'bcryptjs';
import { Controller, Get, Post, Body, Patch, Param, Delete, NotFoundException, UseInterceptors, ClassSerializerInterceptor, Inject, UseGuards } from '@nestjs/common';
import { ApiBadRequestResponse, ApiBearerAuth, ApiBody, ApiCreatedResponse, ApiHeader, ApiNoContentResponse, ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateUser, ReadUser, UpdateUser, User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BadRequestResponse } from 'src/doc/errors';
import { PasswordService } from 'src/password/password.service';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(@InjectRepository(User) private readonly usersService: Repository<User>, @Inject(PasswordService) private readonly password: PasswordService) {}

  @Post()
  @ApiBody({type: CreateUser})
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiCreatedResponse({type: ReadUser})
  @ApiBadRequestResponse({type: BadRequestResponse})
  async create(@Body() createUserDto: CreateUser) {
   const {id, email, lastLogin, createdAt} = await this.usersService.save(this.usersService.create({
      ...createUserDto,
      password: await this.password.hash(createUserDto.password),
      lastLogin: new Date()
    }));
    return { id, email, lastLogin, createdAt }
  }
  
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiOkResponse({type: [ReadUser]})
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  findAll() {
    return this.usersService.find({
      select: ['id','email','lastLogin','createdAt']
    });
  }

  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiOkResponse({type: ReadUser})
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  async findOne(@Param('id') id: string) {
    try {
      return await this.usersService.findOneOrFail({
        select: ['id','email','lastLogin','createdAt'],
        where: {id: +id}
      });
    } catch {
        throw new NotFoundException();
    }
  }

  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiOkResponse({type: ReadUser})
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  async update(@Param('id') id: string, @Body() updateUserDto: UpdateUser) {
    if(updateUserDto.password) {
      updateUserDto.password = await this.password.hash(updateUserDto.password);
    }
    const {email, lastLogin, createdAt} = await this.usersService.save({
      ...(await this.findOne(id)),
      ...updateUserDto,
    });

    return { id, email, lastLogin, createdAt }
  }

  @Delete(':id')
  @ApiNoContentResponse()
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  async remove(@Param('id') id: string) {
    this.usersService.delete([(await this.findOne(id)).id]);
  }
}
