import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocModule } from './doc/doc.module';
import { ValidationPipe } from '@nestjs/common';
import { I18nValidationExceptionFilter, I18nValidationPipe } from 'nestjs-i18n';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);


  app.useGlobalPipes(new ValidationPipe())

  app.useGlobalPipes(new I18nValidationPipe());
  app.useGlobalFilters(new I18nValidationExceptionFilter());


  DocModule.setup(app);
  
  await app.listen(3001);
}
bootstrap();
