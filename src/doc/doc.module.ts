import { Module } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

@Module({})
export class DocModule {

    public static setup(app) {
        const config = new DocumentBuilder()
            .setTitle('TokTok POC')
            .setDescription('Le POC Chez TokTok Doc')
            .setVersion('1.0')
            .addBearerAuth()
            .build();
        const document = SwaggerModule.createDocument(app, config);
        SwaggerModule.setup('', app, document);
    }
}
