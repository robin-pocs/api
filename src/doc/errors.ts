import { ApiProperty } from "@nestjs/swagger"

export class ErrorResponse {
    @ApiProperty()
    public statusCode: number
    @ApiProperty()
    public error: string
};

export class BadRequestResponse extends ErrorResponse{
    @ApiProperty()
    public message: string[]
}