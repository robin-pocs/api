import { ApiProperty, OmitType } from "@nestjs/swagger";
import { UpdateUser, User } from "src/users/entities/user.entity";

export class UserLoginDto {
    @ApiProperty()
    email: string;

    @ApiProperty()
    password: string;
}