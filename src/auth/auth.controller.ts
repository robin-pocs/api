
import { Body, ClassSerializerInterceptor, Controller, Get, Inject, Post, UseGuards, UseInterceptors, Request } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiHeader, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { UserLoginDto } from './dto/user-login.dto';
import { AccessTokenDto } from './dto/access-token.dto';
import { AuthGuard } from './auth.guard';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {

    constructor(@Inject(AuthService) private authService: AuthService){}

    @Post('login')
    @UseInterceptors(ClassSerializerInterceptor)
    @ApiOkResponse({type: AccessTokenDto})
    signIn(@Body() userDto: UserLoginDto) {
      return this.authService.signIn(userDto.email, userDto.password);
    }
}
