import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { I18nService } from 'nestjs-i18n';
import { PasswordService } from 'src/password/password.service';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private readonly users: Repository<User>, 
        @Inject(PasswordService) private readonly password: PasswordService,
        @Inject(I18nService) private readonly i18n: I18nService,
        @Inject(JwtService) private readonly jwtService: JwtService
    ){}

    async signIn(email: string, password: string) {
        const user = await this.users.findOneBy({email});
        if(!user) {
            throw new UnauthorizedException(this.i18n.t('validation.UNAUTHORIZED'));
        }
        const hash = user.password;
        if(!await this.password.verify(password, hash)) {
            throw new UnauthorizedException(this.i18n.t('validation.UNAUTHORIZED'));
        }

        const payload = { sub: user.id, email: user.email };
        return {
            id: user.id,
            access_token: await this.jwtService.signAsync(payload),
          };;

    }
}
