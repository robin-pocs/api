
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'mariadb',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'toktok-poc',
      entities: [],
      autoLoadEntities: true,
      synchronize: true,
    }),
  ],
  exports: [TypeOrmModule]
})
export class DatabaseModule {}
